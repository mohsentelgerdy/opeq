package com.opec.exam.view

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.opec.exam.Application
import com.opec.exam.R
import com.opec.exam.databinding.ActivityMainBinding
import com.opec.exam.databinding.CircleBinding
import com.opec.exam.utils.*
import com.tdco.negarkhone.dagger.factory.MainViewModelFactory
import com.tdco.negarkhone.viewmodel.MainViewModel
import java.lang.Exception
import java.util.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    private var navController: NavController? = null
    private lateinit var binding: ActivityMainBinding


    @Inject
    lateinit var movieViewModelFactory: MainViewModelFactory


    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        injectDagger();

        createViewModel();

        setBinding()

        mainViewModel.reCreateView(binding.parent)
    }

    private fun injectDagger() {
        Application.instance.mApiComponent.inject(this)
    }

    private fun createViewModel() {
        mainViewModel =
            ViewModelProviders.of(this, movieViewModelFactory)[MainViewModel::class.java]
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setBinding() {

        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.lifecycleOwner = this


        binding.add.setOnClickListener(View.OnClickListener {
            try {

                val circleBinding: CircleBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.circle,
                    null,
                    false
                )

                var result = mainViewModel.addView(binding.parent, circleBinding.root)

                if(result == false)
                    mainViewModel.displayMessage(binding.root , getString(R.string.messageFullScreen))
//                mainViewModel.addView(binding.content, circleBinding.root)
            } catch (error: Exception) {
                Log.i(Log.ERROR.toString(), error.message.toString())
            }
        })

        binding.clear.setOnClickListener(View.OnClickListener {
            try {
                mainViewModel.clear(binding.parent)
            } catch (error: Exception) {
                Log.i(Log.ERROR.toString(), error.message.toString())
            }
        })

        binding.sortvertical.setOnClickListener(View.OnClickListener {
            try {
                mainViewModel.sortViewVertical(binding.parent)
            } catch (error: Exception) {
                Log.i(Log.ERROR.toString(), error.message.toString())
            }
        })

        binding.sortHorizontal.setOnClickListener(View.OnClickListener {
            try {
                mainViewModel.sortViewHorizontal(binding.parent)
            } catch (error: Exception) {
                Log.i(Log.ERROR.toString(), error.message.toString())
            }
        })


        setContentView(binding.root)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        try {
            Handler(Looper.getMainLooper()).postDelayed({
                mainViewModel.sortViewHorizontal(binding.parent)
            }, 100)

        } catch (error: Exception) {
            Log.i(Log.ERROR.toString(), error.message.toString())
        }
    }
}

