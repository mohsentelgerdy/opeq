package com.tdco.negarkhone.viewmodel


import android.content.Context
import android.os.Build
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModel
import com.opec.exam.repository.MainRepository


class MainViewModel( private var mainRepository : MainRepository) : ViewModel() {

    @RequiresApi(Build.VERSION_CODES.M)
    fun addView(parent: View, content: View): Boolean
    {
        return mainRepository.addView(parent, content)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun reCreateView(parent: View)
    {
        mainRepository.reCreateView(parent)
    }

    fun sortViewHorizontal(parent: View)
    {

        mainRepository.sortViewHorizontal(parent)
    }

    fun sortViewVertical(parent: View)
    {
        mainRepository.sortViewVertical(parent)
    }


    fun clear(parent: View)
    {
        mainRepository.clear(parent)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun addViewDsl(context: Context, parent: View, content: View)
    {
        mainRepository.addViewDsl(context, parent, content)
    }

    fun sortViewHorizontalDsl(parent: View)
    {

        mainRepository.sortViewHorizontalDsl(parent)
    }

    fun sortViewVerticalDsl(parent: View)
    {
        mainRepository.sortViewVerticalDsl(parent)
    }

    public fun displayMessage(parent : View,message : String) {
        try{
            mainRepository.displayMessage(parent, message)
        } catch (err: Exception) {
            err.message?.let { Log.i(" error:", it) }
        }
    }
}
