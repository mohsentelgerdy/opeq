package com.opec.exam.utils

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.opec.exam.R


class SnackBar() {

    private constructor(builder: Builder) : this()

    class Builder {
        fun build() = SnackBar(this)
    }

    @SuppressLint("ResourceAsColor")
    fun onSNACK(view: View, message: String) {
        //Snackbar(view)
        val snackbar = Snackbar.make(
            view, message,
            Snackbar.LENGTH_LONG
        ).setAction("Action", null)
        snackbar.setActionTextColor(Color.BLUE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(R.color.black)
        val textView =
            snackbarView.findViewById(R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 20f
        snackbar.show()
    }
}
