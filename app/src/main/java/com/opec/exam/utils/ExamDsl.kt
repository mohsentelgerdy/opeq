package com.opec.exam.utils

import android.content.Context
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi

class ExamDsl {
    private var exam: Exam? = null
    var context: Context? = null
    var parent: View? = null
    var content: View? = null

    inline fun with(context: () -> Context) {
        this.context = context()
    }

    inline fun parent(parent: () -> View) {
        this.parent = parent()
    }

    inline fun content(content: () -> View) {
        this.content = content()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun addView() =
        this.parent?.let {
        this.content?.let { it1 ->
                if (this.exam == null)
                    this.exam = Exam(context!!)

                this.exam!!.addView(it, it1)
        }
    }

    fun sortViewHorizontal() = this.parent?.let {
        if (this.exam == null)
            this.exam = Exam(context!!)

        this.exam!!.sortViewHorizontal(it)
    }

    fun sortViewVertical() = this.parent?.let {
        if (this.exam == null)
            this.exam = Exam(context!!)

        this.exam!!.sortViewVertical(it)
    }
}

fun examDslView(lambda: ExamDsl.() -> Unit) =
    ExamDsl() // 1
        .apply(lambda)                     // 2

