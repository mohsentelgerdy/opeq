package com.opec.exam.utils

import android.view.View
import com.opec.exam.databinding.CircleBinding

data class Circle(val circle : View, var x : Float, var y : Float)