package com.opec.exam.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getColor
import androidx.databinding.DataBindingUtil
import com.opec.exam.R
import com.opec.exam.databinding.CircleBinding


class Exam(val context: Context) {
    private var objects: MutableList<View> = mutableListOf<View>();
    private var selectObjects: MutableList<Circle> = mutableListOf<Circle>();

    var vw =  300
    var vh =  300
    /** this function horizontal sort all content view*/
    fun clear(parent: View) {
        objects.clear()
        selectObjects.clear()
        (parent as ViewGroup).removeAllViews()
    }

    /** this function horizontal sort all content view*/
    fun sortViewHorizontal(parent: View) {

        selectObjects.clear()
        objects.forEachIndexed { index, el ->
            el.setBackgroundColor(getColor(context, R.color.white))
        }


        var globalx = 0.0
        var globaly = 0.0
        var globalIndex = 0
        objects.forEachIndexed { index, el ->
            if (globalx + ((globalIndex + 1) * vw) < parent.width) {
                el.x = (globalIndex * vw).toFloat()
                el.y = globaly.toFloat()
                globalIndex++
            } else {
                globalIndex = 0
                globaly = globaly + vh

                el.x = (globalIndex * vw).toFloat()
                el.y = globaly.toFloat()
            }
        }

    }


    /** this function vertical sort all content view*/
    fun sortViewVertical(parent: View) {

        selectObjects.clear()
        objects.forEachIndexed { index, el ->
            el.setBackgroundColor(getColor(context, R.color.white))
        }

        var globalx = 0.0
        var globaly = 0.0
        var globalIndex = 0
        objects.forEachIndexed { index, el ->
            if (globaly + ((globalIndex + 1) * vh) < parent.height) {
                el.x = globalx.toFloat()
                el.y = (globalIndex * vh).toFloat()
                globalIndex++
            } else {
                globalIndex = 0
                globalx = globalx + vw

                el.x = globalx.toFloat()
                el.y = (globalIndex * vh).toFloat()
            }
        }

    }

    /** this function add view in the parent view*/
    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.M)
    fun addView(parent: View, circleBinding: View): Boolean {
        var dX = 0
        var dY = 0
        var moveEvent: Boolean = false
        circleBinding.setOnTouchListener { view, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    /** save start postion drag*/
                    Log.d(Log.INFO.toString(), "-------ACTION_DOWN-------")
                    dX = (view.x - event.rawX).toInt()
                    dY = (view.y - event.rawY).toInt()
                }


                MotionEvent.ACTION_MOVE -> {
                    try {
                        Log.i(Log.INFO.toString(), "-------ACTION_MOVE-------")
                        /** check for multi drag or one object drag*/
                        moveEvent = true

                        var selected = false
                        selectObjects.forEachIndexed { index, el ->
                            if (el.circle == circleBinding) {
                                selected = true
                            }
                        }
                        /** multi drag object*/
                        if (selected && objects.size > 0) {
                            /** prevent from  go out of page*/
                            var preventXMAX = false
                            var preventYMAX = false
                            var preventXMIN = false
                            var preventYMIN = false
                            selectObjects.forEach { el ->
                                if (event.rawX + dX + el.x + circleBinding.width > parent.width)
                                    preventXMAX = true
                            }
                            selectObjects.forEach { el ->
                                if (event.rawY + dY + el.y + circleBinding.height > parent.height)
                                    preventYMAX = true
                            }
                            selectObjects.forEach { el ->
                                if (event.rawX + dX + el.x < 0)
                                    preventXMIN = true
                            }
                            selectObjects.forEach { el ->
                                if (event.rawY + dY + el.y < 0)
                                    preventYMIN = true
                            }

                            selectObjects.forEachIndexed { index, el ->
                                var newX = event.rawX + dX + el.x
                                var newY = event.rawY + dY + el.y
                                if (preventXMAX)
                                    newX = parent.width - circleBinding.width + el.x
                                if (preventYMAX)
                                    newY = parent.height - circleBinding.height + el.y
                                if (preventXMIN)
                                    newX = el.x
                                if (preventYMIN)
                                    newY = el.y
                                el.circle.animate().x(newX).y(newY).setDuration(0).start()
                            }
                            preventXMAX = false
                            preventYMAX = false
                            preventXMIN = false
                            preventYMIN = false
                        } else {
                            /** one drag object*/
                            var newX = event.rawX + dX
                            var newY = event.rawY + dY
                            /** prevent from  go out of page*/
                            if (newX + circleBinding.width > parent.width)
                                newX = (parent.width - circleBinding.width).toFloat()
                            if (newY + circleBinding.height > parent.height)
                                newY = (parent.height - circleBinding.height).toFloat()
                            if (newX < 0)
                                newX = 0F
                            if (newY < 0)
                                newY = 0F
                            view.animate().x(newX).y(newY).setDuration(0).start()
                        }
                    } catch (err: Exception) {
                        Log.i("error in ACTION_MOVE", err.message.toString())
                    }
                }

                MotionEvent.ACTION_UP -> {
                    try {
                        if (!moveEvent) {
                            Log.i(Log.INFO.toString(), "-------ACTION_UP-------")
                            val endX = event.x
                            val endY = event.y
                            if (isAClick(startX, endX, startY, endY)) {
                                /** find object*/
                                var selectedCircleBinding: View? = null
                                selectObjects.forEachIndexed { index, el ->
                                    if (el.circle == circleBinding)
                                        selectedCircleBinding = el.circle
                                }

                                if (selectedCircleBinding == null) {
                                    /** select object*/
                                    view.setBackgroundColor(getColor(context, R.color.blue))
                                    var circle =
                                        Circle(
                                            circleBinding,
                                            circleBinding.x,
                                            circleBinding.y
                                        )
                                    selectObjects.add(circle)
                                } else {
                                    /** deselect object*/
                                    circleBinding.setBackgroundColor(
                                        getColor(
                                            context,
                                            R.color.white
                                        )
                                    )
                                    selectObjects.forEachIndexed { index, el ->
                                        if (el.circle == selectedCircleBinding)
                                            selectObjects.removeAt(index)
                                    }

                                }
                            }
                        } else
                            moveEvent = false
                    } catch (err: Exception) {
                        Log.i("error in ACTION_UP", err.message.toString())
                    }
                }
            }

            return@setOnTouchListener true
        }


        /** set postion and add view to paent */
        if (objects.size > 0) {
            /** check for count object in page*/
            var acceptedCountObject =
                (parent.width * parent.height) / (objects.get(0).width * objects.get(
                    0
                ).height)
            if (objects.size + 1 > acceptedCountObject)
                return false

            var objectX = objects.get(objects.lastIndex).x
            var objectY = objects.get(objects.lastIndex).y
            var objectW = objects.get(objects.lastIndex).width
            var objectH = objects.get(objects.lastIndex).height
            vw =  objects.get(objects.lastIndex).width
            vh =  objects.get(objects.lastIndex).height
            var parentW = parent.width
            var parentH = parent.height
            if (objectX + (2 * objectW) < parentW) {
                circleBinding.x = objectX.toFloat() + objectW
                circleBinding.y = objectY.toFloat()
            } else {
                circleBinding.x = 0F
                circleBinding.y = objectY.toFloat() + objectH
            }
        }
        (parent as RelativeLayout).addView(circleBinding);

        /** add to list*/
        objects.add(circleBinding)
        return true

    }

    /** this function add view in the parent view*/
    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.M)
    fun reCreateView(parent: View) {
        if (objects.size > 0) {
            (parent as ViewGroup).removeAllViews()
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var globalx = 0.0
            var globaly = 0.0
            var globalIndex = 0
            var countObject = objects.size
            objects.clear()

            for (i in 0..countObject - 1) {

                val circleBinding: CircleBinding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.circle,
                    null,
                    false
                )

                var dX = 0
                var dY = 0
                var moveEvent: Boolean = false
                circleBinding.root.setOnTouchListener { view, event ->
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            /** save start postion drag*/
                            Log.d(Log.INFO.toString(), "-------ACTION_DOWN-------")
                            dX = (view.x - event.rawX).toInt()
                            dY = (view.y - event.rawY).toInt()
                        }


                        MotionEvent.ACTION_MOVE -> {
                            try {
                                Log.i(Log.INFO.toString(), "-------ACTION_MOVE-------")
                                /** check for multi drag or one object drag*/
                                moveEvent = true

                                var selected = false
                                selectObjects.forEachIndexed { index, el ->
                                    if (el.circle == circleBinding.root) {
                                        selected = true
                                    }
                                }
                                /** multi drag object*/
                                if (selected && objects.size > 0) {
                                    /** prevent from  go out of page*/
                                    var preventXMAX = false
                                    var preventYMAX = false
                                    var preventXMIN = false
                                    var preventYMIN = false
                                    selectObjects.forEach { el ->
                                        if (event.rawX + dX + el.x + circleBinding.root.width > parent.width)
                                            preventXMAX = true
                                    }
                                    selectObjects.forEach { el ->
                                        if (event.rawY + dY + el.y + circleBinding.root.height > parent.height)
                                            preventYMAX = true
                                    }
                                    selectObjects.forEach { el ->
                                        if (event.rawX + dX + el.x < 0)
                                            preventXMIN = true
                                    }
                                    selectObjects.forEach { el ->
                                        if (event.rawY + dY + el.y < 0)
                                            preventYMIN = true
                                    }

                                    selectObjects.forEachIndexed { index, el ->
                                        var newX = event.rawX + dX + el.x
                                        var newY = event.rawY + dY + el.y
                                        if (preventXMAX)
                                            newX = parent.width - circleBinding.root.width + el.x
                                        if (preventYMAX)
                                            newY = parent.height - circleBinding.root.height + el.y
                                        if (preventXMIN)
                                            newX = el.x
                                        if (preventYMIN)
                                            newY = el.y
                                        el.circle.animate().x(newX).y(newY).setDuration(0).start()
                                    }
                                    preventXMAX = false
                                    preventYMAX = false
                                    preventXMIN = false
                                    preventYMIN = false
                                } else {
                                    /** one drag object*/
                                    var newX = event.rawX + dX
                                    var newY = event.rawY + dY
                                    /** prevent from  go out of page*/
                                    if (newX + circleBinding.root.width > parent.width)
                                        newX = (parent.width - circleBinding.root.width).toFloat()
                                    if (newY + circleBinding.root.height > parent.height)
                                        newY = (parent.height - circleBinding.root.height).toFloat()
                                    if (newX < 0)
                                        newX = 0F
                                    if (newY < 0)
                                        newY = 0F
                                    view.animate().x(newX).y(newY).setDuration(0).start()
                                }
                            } catch (err: Exception) {
                                Log.i("error in ACTION_MOVE", err.message.toString())
                            }
                        }

                        MotionEvent.ACTION_UP -> {
                            try {
                                if (!moveEvent) {
                                    Log.i(Log.INFO.toString(), "-------ACTION_UP-------")
                                    val endX = event.x
                                    val endY = event.y
                                    if (isAClick(startX, endX, startY, endY)) {
                                        /** find object*/
                                        var selectedCircleBinding: View? = null
                                        selectObjects.forEachIndexed { index, el ->
                                            if (el.circle == circleBinding.root)
                                                selectedCircleBinding = el.circle
                                        }

                                        if (selectedCircleBinding == null) {
                                            /** select object*/
                                            view.setBackgroundColor(getColor(context, R.color.blue))
                                            var circle =
                                                Circle(
                                                    circleBinding.root,
                                                    circleBinding.root.x,
                                                    circleBinding.root.y
                                                )
                                            selectObjects.add(circle)
                                        } else {
                                            /** deselect object*/
                                            circleBinding.root.setBackgroundColor(
                                                getColor(
                                                    context,
                                                    R.color.white
                                                )
                                            )
                                            selectObjects.forEachIndexed { index, el ->
                                                if (el.circle == selectedCircleBinding)
                                                    selectObjects.removeAt(index)
                                            }

                                        }
                                    }
                                } else
                                    moveEvent = false
                            } catch (err: Exception) {
                                Log.i("error in ACTION_UP", err.message.toString())
                            }
                        }
                    }

                    return@setOnTouchListener true
                }

                /** set postion and add view to paent */
                if (objects.size > 0) {
                    if (globalx + ((globalIndex + 1) * vw) < parent.width) {
                        circleBinding.root.x = (globalIndex * vw).toFloat()
                        circleBinding.root.y = globaly.toFloat()
                        globalIndex++
                    } else {
                        globalIndex = 0
                        globaly = globaly + vh

                        circleBinding.root.x = (globalIndex * vw).toFloat()
                        circleBinding.root.y = globaly.toFloat()
                    }
                }

                (parent as RelativeLayout).addView(circleBinding.root);
                /** add to list*/
                objects.add(circleBinding.root)
            }
        }
    }

    /** detect click function*/
    val CLICK_ACTION_THRESHOLD = 200
    val startX = 0f
    val startY = 0f
    fun isAClick(startX: Float, endX: Float, startY: Float, endY: Float): Boolean {
        val differenceX = Math.abs(startX - endX)
        val differenceY = Math.abs(startY - endY)
        return !(differenceX > CLICK_ACTION_THRESHOLD /* =5 */ || differenceY > CLICK_ACTION_THRESHOLD)
    }

    fun pxToDp(px: Int): Int {
        val displayMetrics: DisplayMetrics = context.getResources().getDisplayMetrics()
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }
}