package com.opec.exam.dagger.module

import android.content.Context
import com.opec.exam.utils.SnackBar
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ToolBarModule(context: Context) {
    @Singleton
    @Provides
    fun providesSnackBar(): SnackBar {
        return SnackBar()
    }
}