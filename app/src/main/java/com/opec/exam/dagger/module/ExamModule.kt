package com.tdco.negarkhone.dagger.module

import android.content.Context
import com.opec.exam.repository.MainRepository
import com.opec.exam.utils.Exam
import com.opec.exam.utils.ExamDsl
import com.opec.exam.utils.SnackBar
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ExamModule(context: Context) {
    @Singleton
    @Provides
    fun providesExam(context: Context) = Exam(context)

    @Singleton
    @Provides
    fun providesMainRepository(exam: Exam , snackBar: SnackBar) = MainRepository(exam , snackBar)
}
