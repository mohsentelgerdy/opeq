package com.tdco.negarkhone.dagger.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(mApplication: Application) {
    private val mApplication: Application
    @get:Singleton
    @get:Provides
    val application: Application
        get() = mApplication

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = application

    init {
        this.mApplication = mApplication
    }
}
