package com.tdco.negarkhone.dagger.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.opec.exam.repository.MainRepository
import com.tdco.negarkhone.viewmodel.MainViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Suppress("UNCHECKED_CAST")
@Singleton
class MainViewModelFactory @Inject constructor(
    private  var mainRepository: MainRepository,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(
            mainRepository
            )  as T
    }
}