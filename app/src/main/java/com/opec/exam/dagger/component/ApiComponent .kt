package com.tdco.negarkhone.dagger.component

import com.opec.exam.dagger.module.ToolBarModule
import com.opec.exam.view.MainActivity
import com.tdco.negarkhone.dagger.module.*
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class,ExamModule::class,ToolBarModule::class])
interface ApiComponent {
    fun inject(mainActivity: MainActivity?)
}