package com.opec.exam

import android.app.Application
import com.opec.exam.dagger.module.ToolBarModule
import com.tdco.negarkhone.dagger.component.*
import com.tdco.negarkhone.dagger.module.*
import java.util.*


class Application : Application(){

    companion object {
        lateinit var instance: com.opec.exam.Application
    }
    lateinit var mApiComponent: ApiComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        mApiComponent = DaggerApiComponent.builder()
            .appModule(AppModule(this))
            .examModule(ExamModule(this))
            .toolBarModule(ToolBarModule(this))
            .build();

    }
}