package com.opec.exam.repository

import android.content.Context
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.opec.exam.utils.Exam
import com.opec.exam.utils.ExamDsl
import com.opec.exam.utils.SnackBar
import com.opec.exam.utils.examDslView


class MainRepository(
    var exam: Exam,
    var snackBar: SnackBar,
) {
    @RequiresApi(Build.VERSION_CODES.M)
    fun addView(parent: View, content: View) : Boolean
    {
        return exam.addView(parent, content)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun reCreateView(parent: View)
    {
        return exam.reCreateView(parent)
    }

    fun clear(parent: View)
    {
        exam.clear(parent)
    }

    fun sortViewHorizontal(parent: View)
    {
        exam.sortViewHorizontal(parent)
    }

    fun sortViewVertical(parent: View)
    {
        exam.sortViewVertical(parent)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun addViewDsl(context: Context, parent: View, content: View)
    {
        examDslView{
            this.context = context
            this.parent = parent
            this.content = content
            addView()
        }
    }

    fun sortViewHorizontalDsl(parent: View)
    {
        examDslView {
            sortViewHorizontal()
        }
    }

    fun sortViewVerticalDsl(parent: View)
    {
        examDslView {
            sortViewVertical()
        }
    }

    fun displayMessage(view: View, message: String)
    {
        snackBar.onSNACK(view, message)
    }
}
